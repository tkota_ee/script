def slack(index_page):
    from slackclient import SlackClient
    import yaml

    f = open("./config/slack.yml", "r+")
    data = yaml.load(f)
    slack_token = data['token']
    sc = SlackClient(slack_token)

    sc.api_call(
        "chat.postMessage",
        channel="#channel",
        name="thumbsup",
        timestamp="1234567890.123456",
        text=["Done!!",index_page,datetime.now().strftime("%Y/%m/%d %H:%M:%S")],

        )
    return