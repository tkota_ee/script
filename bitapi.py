# -*- coding: utf-8 -*-
import pybitflyer
import json

def main():
    
    api = pybitflyer.API(api_key="************",
                         api_secret="**************")
    b = api.board(product_code="BTC_JPY")
    t = api.ticker(product_code="BTC_JPY")
    print(json_shaping(t))
    
def json_shaping(j) :
    dict = j
    format_json = json.dumps(dict, indent=4, separators=(',', ': '))
    return format_json

if __name__ == '__main__':
main()