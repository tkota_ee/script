# -*- coding: utf-8 -*-
import hashlib as hash

# ハッシュを生成するためのアルゴリズムを定義する
sha = hash.sha256()

# hello world??という文字列をエンコードして、ハッシュ化する
sha.update('hello world??'.encode('utf-8'))

# hello world??のハッシュ値を表示
print('hello world??',sha.hexdigest())

# 比較のため?なしバージョンも
sha.update('hello world'.encode('utf-8'))
print('hello world', sha.hexdigest())
