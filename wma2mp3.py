# -*- coding: utf-8 -*-
#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import fnmatch
from subprocess import call

matches = []
for root, dirnames, filenames in os.walk('./'):
    for filename in fnmatch.filter(filenames,'*.wma'):
        matches.append(os.path.join(os.path.abspath(root), filename))

for match in matches:
    message = "convert {0} to {1}".format(match, match.replace('.wma', '.mp3'))
    before = match.decode('utf-8')
    after = before.replace('.wma', '.mp3')
    print message
    if os.path.exists(after):
        print 'This file is already converted.'
    else:
call(u'ffmpeg -i "{0}" "{1}"'.format(before, after), shell=True)