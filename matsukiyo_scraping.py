# -*- coding: utf-8 -*-
# !/usr/bin/env/python
# coding: utf-8

from urllib.request import urlretrieve
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from time import time
import requests, random, datetime, re, os, json
import time
import json
import sys
import pandas as pd
sys.setrecursionlimit(10000)

# 001:医薬品 p216
# 003:化粧品 p334
# 004:日用品 p334
# 005:食品 p165
print("\n001:医薬品p216\n 003:化粧品p334\n 004:日用品p334\n 005:食品p165\n")
mainurl = "http://www.matsukiyo.co.jp"
baseurl = "http://www.matsukiyo.co.jp/store/online"
downloadDirectory = "./downloads"
index_page_list = []
title_list = []
summary_url_list = []
img_list = []
i = 0
dict = {}

pnum = input('choose pnum--> ')
pages = int(input('how many pages?--> '))
index_url = "http://www.matsukiyo.co.jp/store/corp/c/%s?page=" % pnum

def main():
    for i in range(pages):
        index_page = pageget(index_page_list, i)
        print('\033[35m', ">>")
        print('\033[35m', index_page)
        print('\033[35m', "<<")
        try:
            # get soup
            data(index_page)
        except Exception as e:
            print(e)
        # get title&url
        titleurl_get(index_page_list, data(index_page))
    # json to csv
    json2csv()
    print('\033[31m','\033[42m','\033[1m', "Create csv [SUCCESS]!!", '\033[0m')


def pageget(index_page_list, i):
    index_page_list = index_url + str(i)
    time.sleep(0.5)
    return index_page_list

def data(index_page):
    # urlをsoupに
    result = requests.get(index_page)
    # print(index_page)
    c = result.content
    soup = BeautifulSoup(c,"html.parser")
    return soup

def titleurl_get(index_page_list, soup):
    # get title&url
    items = soup.find_all('h4',{'class':'itemContainer__title'})
    num = 0

    for title in items:
        num+=1
        product_name = title.text.strip()
        print('\033[94m',"No.", num, "product_name: ",'\033[33m', product_name,'\033[0m')
        id = title.a.get("href")[-13:]
        url = baseurl + "/p/" + id
        print('\033[94m',"product_id: ",'\033[33m', id)
        print('\033[94m',"product_url: ", '\033[33m', url)
        img_url = imgurl_get(index_page_list, soup, id)
        print('\033[94m',"img_url: ", '\033[33m',img_url)

        print('\033[36m',">-------------------------------<")
        # get img file
        # try:
        #     get_img(id, img_url, downloadDirectory)
        #     print('\033[31m','\033[42m','\033[1m', "get_img success!!", '\033[0m')
        # except Exception as e:
        #     print(e)
        # make json file
        dict.update({id:{
            "id":id, "img_url":img_url, "product_name":product_name, "url":url
        }})

    f = open('%s.json'% pnum, 'w')
    json.dump(dict, f, indent=4, ensure_ascii=False)
    print('\033[31m','\033[42m','\033[1m', "crate json success!!", '\033[0m')


def imgurl_get(index_page_list, soup, id):
    # get title&url
    imgs = soup.find_all('img',{'class':'fixedImg'}, src=re.compile(id))
    num = 0
    for img in imgs:
        num+=1
        img_url = mainurl + img.get('src')
        return img_url

def get_img(id, img_url, downDirectory):
    import subprocess
    cmd = "wget -O ./downloads/%s.jpg %s" %(id, img_url)
    subprocess.call( cmd.strip().split(" ")  )

def json2csv():
    with open('$s.json'% pnum, 'r') as f :
        data_json = json.load(f)
    json_pd = pd.DataFrame(data_json)
    json_pd_t = json_pd.T
    json_pd_t.to_csv("%s.csv"% pnum)

if __name__ == '__main__':
main()