# -*- coding: utf-8 -*-
# !/usr/bin/env/python
# coding: utf-8

from urllib.request import urlretrieve
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import bs4
from time import time
import requests, random, datetime, re, os, json
import itertools
import urllib.request
import time
import json
import sys
import pandas as pd
from subprocess import getoutput  
from datetime import datetime
sys.setrecursionlimit(10000)

downloadDirectory = "./downloads"
index_page_list = []
title_list = []
summary_url_list = []
img_list = []
i = 0
dict = {}

# 引数定義
type = sys.argv[1]
from_pages =  int(sys.argv[2])
to_pages =  int(sys.argv[3])
# URL定義
base_data={'movie':1,'music':2,'book':3,'game':4}
print(base_data)
index_url = "https://tsutaya.tsite.jp/search/?dm=0&p="
print(base_data[type])
baseurl = "https://tsutaya.tsite.jp/item/%s/"%type 
ic = "&ic=%s"%base_data[type]

def main():
    for i in range(from_pages, to_pages):
        index_page = pageget(index_page_list, i, ic)
        print('\033[35m', ">>")
        print('\033[35m', index_page)
        print('\033[35m', "<<")
        try:
            # get soup
            data(index_page)
        except Exception as e:
            print("error1: ", e)
        try:
            # get title&url
            titleurl_get(index_page_list, data(index_page), index_page)
        except Exception as e:
            print("error2: ", e)

    # json to csv
    json2csv()
    print('\033[31m','\033[42m','\033[1m', "Create csv [SUCCESS]!!", '\033[0m')


def pageget(index_page_list, i, ic):
    index_page_list = index_url + str(i) + ic
    return index_page_list

def data(index_page):
    # urlをsoupに
    response = urllib.request.urlopen(index_page)
    data = response.read()
    # print(index_page)
    soup = bs4.BeautifulSoup(data, "lxml")
    return soup

def titleurl_get(index_page_list, soup, index_page):
    # get title&url
    items = soup.find_all('p',{'class':'c_thumb_tit'})
    imgs = soup.find_all('span',{'class':'c_thumb_list_row-img_in'})

    num = 0

    for title, img in itertools.zip_longest(items, imgs):
        num+=1
        product_name = title.text.strip()
        print('\033[94m',"No.", num, "product_name: ",'\033[33m', product_name,'\033[0m')
        img_ = img.find('img')
        img_url = img_.get('src')
        id_ = title.a.get("href")
        _id = id_.split("?")[0]
        add_id = _id.split("/")[2]
        id_withjpg = img_url.split("/")[-1]
        id = id_withjpg.split(".")[-2]
        url = baseurl + add_id
        print('\033[94m',"product_id: ",'\033[33m', id)
        print('\033[94m',"product_url: ", '\033[33m', url)

        print('\033[94m',"img_url: ", '\033[33m',img_url)
        print('\033[36m',">-------------------------------<")
        # get img file
        try:
            urlretrieve(img_url, "./downloads/" + img_url.split("/")[-1])
            print('\033[31m','\033[42m','\033[1m', "get_img success!!", '\033[0m')
        except Exception as e:
            print(e)
        # make json file
        dict.update({id:{
            "id":id, "img_url":img_url, "product_name":product_name, "url":url
        }})

    f = open("./%s-%s-%s.json"%(str(type), from_pages, to_pages), 'w')
    json.dump(dict, f, indent=4, ensure_ascii=False)
    print('\033[31m','\033[42m','\033[1m', "crate json success!!", '\033[0m')
    slack(index_page)

def imgurl_get(index_page_list, soup, id):
    # get title&url
    imgs = soup.find_all('img',{'class':'fixedImg'}, src=re.compile(id))
    num = 0
    for img in imgs:
        num+=1
        img_url = mainurl + img.get('src')
        return img_url

def slack(index_page):
    from slackclient import SlackClient
    import yaml

    f = open("./config/slack.yml", "r+")
    data = yaml.load(f)
    slack_token = data['token']
    sc = SlackClient(slack_token)

    sc.api_call(
        "chat.postMessage",
        channel="#kot_test",
        name="thumbsup",
        timestamp="1234567890.123456",
        text="\n%s ```\n'df-h: '%s\n'URL: ' %s\n'timestamp: '%s ``` `DONE!!`\n"%('>--------------------------------------<', getoutput('df -h'), index_page, datetime.now().strftime("%Y/%m/%d %H:%M:%S"))
        )
    return

def json2csv():
    with open("./%s-%s-%s.json"%(str(type), from_pages, to_pages), 'r') as f :
        data_json = json.load(f)
    json_pd = pd.DataFrame(data_json)
    json_pd_t = json_pd.T
    json_pd_t.to_csv("%s-%s-%s.csv"% (str(type), from_pages, to_pages))

def get_img(id, img_url, downDirectory):
    import subprocess
    cmd = "wget -O ./downloads/%s.jpg %s" %(id, img_url)
    subprocess.call( cmd.strip().split(" ")  )


if __name__ == '__main__':
main()