#!/.pyenv/shims/python
# -*- coding: utf-8 -*-

import feedparser
import ssl

#RSS URL
RSS_URL  = "http://secbeg.hatenablog.com/rss"

#SSL
if hasattr(ssl, '_create_unverified_context'):
    ssl._create_default_https_context = ssl._create_unverified_context
#RSS取得
feed = feedparser.parse(RSS_URL)

#RSS title
print(feed.feed.title)

#RSS  published link
for entry in feed.entries:
    print(entry.published)
    print(entry.link)